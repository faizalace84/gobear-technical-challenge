#!/bin/bash

#Removes existing stack
docker-compose -f docker-compose.yml down > /dev/null 2>&1

# Image build takes a long time.Pulling pre-built image from my repo to save time.
#docker build -t mydrupal .
docker pull faizalyusuf/mydrupal:1.0
 
docker-compose -f docker-compose.yml up -d 