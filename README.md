# Technical Challenge Solution
---

## Deploying the Drupal Stack Locally

#### Requirements:

1. Linux or MacOS
2. [Docker CE](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
3. [Docker Compose](https://docs.docker.com/compose/install/)

#### To deploy the stack: 

1. Clone this repository. 
2. Run `./deploy-stack.sh`
3. Go to [localhost:8080](http://localhost:8080) to access Drupal's initial configuration page.
4. run `./remove-stack.sh` to remove the stack.

---

## CI/CD

* This deploys the stack through a Jenkins pipeline. Pipeline configuration is in the [Jenkinsfile](Jenkinsfile). 
* It will detect any changes to the repository and trigger an automated build and deploy of the stack to a docker swarm cluster.  
* A new database user and password are created through swarm's docker secrets feature see [docker-compose.ci.yml](docker-compose.ci.yml). 
* CI/CD builds and secrets for this application are managed centrally through my [Jenkins server](http://35.240.168.28:8081/) (Login with username/password:**testuser**/**testuser123**). 

#### CI/CD Example:

1. The Drupal application is currently running on a single container [here](http://35.240.168.28:8080/) (login: **user/password**)
2. Clone the repository.
3. Login to the my [portainer docker visualizer](http://35.240.168.28:9000/#/swarm/visualizer) with username/password:**admin**/**password**. Select the "local" endpoint, then click on "Go to cluster visualizer" to view the number of running drupal container replicas (currently 1).
4. In [docker-compose.ci.yml](docker-compose.ci.yml), increase the number of replicas in the drupal service to 2.
5. Commit and push your changes. This would trigger a new build in [Jenkins](http://35.240.168.28:8081). 
6. Once the Jenkins build has completed, return to the [portainer docker visualizer](http://35.240.168.28:9000/#/swarm/visualizer). The number of running drupal replicas have increased to 2 now.

---

## ELK Centralised Logging

* I have set-up centralised log monitoring for the [application](http://35.240.168.28:8080/) on a hosted [ELK instance](https://app.logz.io/#/login) (login username/password: **faizalace84@hotmail.com**/**password**)
* This instance monitors site activity and outputs it to a Kibana dashboard.

1. After login, click on "Kibana"
2. Click on "Dashboard" on the left-hand panel and click on the dashboard "Drupal Site Activity".
3. To ship logs to Elasticsearch and view them in the Kibana dashboard, return to the [application](http://35.240.168.28:8080/) and click random links on the site.
4. Return to Kibana and refresh the dashboard to see the chart depicting site activity.

---

#### References

* https://docs.docker.com/samples/library/mysql/#mysql_random_root_password
* https://www.drupal.org/documentation/install/developers
* https://getcomposer.org/doc/00-intro.md
* https://drupalconsole.com/articles/how-to-install-drupal-console
* https://www.siteground.com/kb/drupal-console/
* http://docs.drush.org/en/master/install/
* https://www.drupal.org/docs/8/configuration-management/managing-your-sites-configuration